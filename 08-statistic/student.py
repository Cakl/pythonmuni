import csv
import sys
import numpy as np
from scipy import stats
import json
import re
import datetime

not_found = 1



def check_param(params, cnt_param):
    if len(params) <= cnt_param + 1:
        return True
    else:
        return False


def find_number_of_data_per_id(data):
    for cnt_item, item in enumerate(data):
        if len(item) > 0:
            if item[0] == sys.argv[2]:
                return cnt_item
    return not_found


def calc_average(data):
    points_data = []
    date_data = []
    pass_ex = 0

    if sys.argv[2] == "average":
        dict_average = {}
        dict_point = {}
        dict_point1 = {}
        for item in data:
            for date in data[item].keys():
                match = re.findall(r"\d{4}-\d{2}-\d{2}/(\d{2})$", date)
                if match:
                    if match[0] in dict_point.keys():
                        dict_point[match[0]] = dict_point[match[0]] + data[item][date]
                    else:
                        dict_point[match[0]] = data[item][date]

                if date in dict_point1.keys():
                    dict_point1[date] = dict_point1[date] + data[item][date]
                else:
                    dict_point1[date] = data[item][date]

        for item in dict_point.keys():
            dict_point[item] = dict_point[item] / len(data)

        for item in dict_point1.keys():
            dict_point1[item] = dict_point1[item] / len(data)

        dict_average['0'] = dict_point

        for item in dict_average.values():
            for item1 in item.values():
                # print(item)
                points_data.append(float(item1))
                if float(item1) > 0.0:
                    pass_ex += 1

        return dict_point1, np.mean(points_data), np.median(points_data), np.sum(points_data), pass_ex

    for item in data[sys.argv[2]].values():
        points_data.append(float(item))
        if float(item) > 0.0:
            pass_ex += 1

    for item in data[sys.argv[2]].keys():
        date_data.append(item)

    return None, np.mean(points_data), np.median(points_data), np.sum(points_data), pass_ex


def data_to_dict_per_id(data):
    all_date = {}
    for item in data[0][1:]:
        match = re.findall(r"(\d{4}-\d{2}-\d{2})/(\d{2})$", item)
        if match:
            if match[0][0] in all_date.keys():
                all_date[match[0][0]] = str(all_date[match[0][0]]) + ',' + str(match[0][1])
            else:
                all_date[match[0][0]] = match[0][1]

    id_dict = {}
    for item in data[1:]:
        date_dict = {}
        exc_view = []
        for cnt_item_date, item_date in enumerate(data[0][1:]):
            excercise = re.findall(r"\d{4}-\d{2}-\d{2}/(\d{2})$", item_date)
            if excercise:
                if excercise in exc_view:
                    continue
                exc_view.append(excercise)
                excercises = []
                excercises_date = []
                for cnt_item_date1, item_date1 in enumerate(data[0][1:]):
                    excercise1 = re.findall(r"(\d{4}-\d{2}-\d{2})/(\d{2})$", item_date1)
                    if excercise1:
                        if excercise[0] == excercise1[0][1]:
                            excercises_date.append(item_date1)
                            excercises.append(float(item[cnt_item_date1 + 1]))
                if sum(excercises) == 0:
                    date_dict[excercises_date[0]] = 0
                    id_dict[item[0]] = date_dict
                else:
                    for cnt_point, point in enumerate(excercises):
                        if point > 0:
                            date_dict[excercises_date[cnt_point]] = point
                            id_dict[item[0]] = date_dict
                            break

    all_date_sorted = {}
    for item in sorted(all_date):
        all_date_sorted[item] = all_date[item]

    return all_date_sorted, id_dict


def calc_slope(date, save_data):
    axis_a = []
    a_base = 0
    semester_start = datetime.datetime(2018, 9, 17)
    days = []
    for item in date:
        match = re.findall(r"(\d{4})-(\d{2})-(\d{2})$", item)
        if match:
            date_calc = datetime.datetime(int(match[0][0]), int(match[0][1]), int(match[0][2]))
            days.append((date_calc - semester_start).days)
    concat_date = []

    for item in date:
        ex_num = date[item].split(',')
        spl_date = []
        for i in ex_num:
            spl_date.append(item + '/' + i)
        concat_date.append(spl_date)

    for item in concat_date:
        calc = 0
        for item1 in item:

            if sys.argv[2] != 'average':
                if item1 in save_data[sys.argv[2]].keys():
                    calc += save_data[sys.argv[2]][item1]
            else:
                if item1 in save_data[sys.argv[2]].keys():
                    calc += save_data[sys.argv[2]][item1]

        axis_a.append(a_base + calc)

        a_base += calc

    sumation = 0
    square = 0
    for i in range(0, len(days)):
        square += days[i] ** 2
        sumation += axis_a[i] * days[i]

    slope = sumation / square

    if slope == 0:
        date16 = "inf"
        date20 = "inf"

        return slope, date16, date20
    else:
        d16 = 16 / slope
        d20 = 20 / slope

        date16 = semester_start + datetime.timedelta(d16)
        date20 = semester_start + datetime.timedelta(d20)

        return slope, date16.date(), date20.date()


def main():
    if check_param(sys.argv, 2):
        with open(sys.argv[1], 'r') as csvfile:
            reader = csv.reader(csvfile)
            data_csv = list(reader)
            if len(sys.argv) == 3:
                all_date, save_data = data_to_dict_per_id(data_csv)

                dict_point, average, median, total, passed = calc_average(save_data)

                if sys.argv[2] == 'average':
                    save_data['average'] = dict_point

                slope, date16, date20 = calc_slope(all_date, save_data)

                print(json.dumps(
                    {"mean": average, "median": median, "total": total, "passed": passed, "regression slope": slope,
                     "date 16": str(date16), "date 20": str(date20)}, indent=2, ensure_ascii=False))


if __name__ == "__main__":
    main()