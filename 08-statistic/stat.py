import csv
import sys
import numpy as np
import json
import re

def check_param(params, cnt_param):
    if len(params) <= cnt_param + 1:
        return True
    else:
        return False

def data_to_dict_per_id(data):
    all_date = {}
    for item in data[0][1:]:
        match = re.findall(r"(\d{4}-\d{2}-\d{2})/(\d{2})$", item)
        if match:
            if match[0][0] in all_date.keys():
                all_date[match[0][0]] = str(all_date[match[0][0]]) + ',' + str(match[0][1])
            else:
                all_date[match[0][0]] = match[0][1]

    id_dict = {}
    for item in data[1:]:
        date_dict = {}
        exc_view = []
        for cnt_item_date, item_date in enumerate(data[0][1:]):
            excercise = re.findall(r"\d{4}-\d{2}-\d{2}/(\d{2})$", item_date)
            if excercise:
                if excercise in exc_view:
                    continue
                exc_view.append(excercise)
                excercises = []
                excercises_date = []
                for cnt_item_date1, item_date1 in enumerate(data[0][1:]):
                    excercise1 = re.findall(r"(\d{4}-\d{2}-\d{2})/(\d{2})$", item_date1)
                    if excercise1:
                        if excercise[0] == excercise1[0][1]:
                            excercises_date.append(item_date1)
                            excercises.append(float(item[cnt_item_date1 + 1]))
                if sum(excercises) == 0:
                    date_dict[excercises_date[0]] = 0
                    id_dict[item[0]] = date_dict
                else:
                    for cnt_point, point in enumerate(excercises):
                        if point > 0:
                            date_dict[excercises_date[cnt_point]] = point
                            id_dict[item[0]] = date_dict
                            break

    all_date_sorted = {}
    for item in sorted(all_date):
        all_date_sorted[item] = all_date[item]

    return all_date_sorted, id_dict


def calc_date(date, data):
    dates = {}
    for item in date.keys():
        date_point=[]
        for item1 in data:
            clc =0
            for item2 in data[item1].keys():
                match = re.findall(r"(\d{4}-\d{2}-\d{2})/\d{2}$", item2)
                if match:
                    if item == match[0]:
                        clc += data[item1][item2]
            date_point.append(clc)
        dates[item] = date_point

    return_dates = {}
    for i in dates:
        total = 0
        for item in dates[i]:
            if item > 0.0:
                total += 1
        return_dates[i] = {"mean": np.mean(dates[i]), "median": np.median(dates[i]), "first":np.percentile(dates[i], 25),"last": np.percentile(dates[i], 75),"passed":total}


    return return_dates

def calc_deadlines(date,data):
    all_deadlines = []
    for item in date.keys():
        deadl = date[item].split(",")
        for i in deadl:
            all_deadlines.append(item + '/' + i)

    deadlines_data = {}

    for item in all_deadlines:
        points = []
        for item1 in data:
            cnt = 0
            for item2 in data[item1]:
                if item == item2:
                    cnt = data[item1][item2]
            points.append(cnt)
        deadlines_data[item] = points

    return_deadlines = {}
    for i in all_deadlines:
        total = 0
        for item in deadlines_data[i]:
            if item > 0.0:
                total += 1
        return_deadlines[i] = {"mean": np.mean(deadlines_data[i]), "median": np.median(deadlines_data[i]), "first":np.percentile(deadlines_data[i], 25),"last": np.percentile(deadlines_data[i], 75), "passed":total}

    return  return_deadlines

def calc_exercises(data):
    all_exercises = []
    for i in data:
        for j in data[i].keys():
            match = re.findall(r"\d{4}-\d{2}-\d{2}/(\d{2})$", j)
            if match:
                all_exercises.append(match[0])
        break

    exercises_data = {}
    for item in all_exercises:
        points = []
        for item1 in data:
            cnt = 0
            for item2 in data[item1]:
                match = re.findall(r"\d{4}-\d{2}-\d{2}/(\d{2})$", item2)
                if match:
                    if match[0] == item:
                        cnt = data[item1][item2]
            points.append(cnt)
        exercises_data[item] = points

    return_exercises = {}
    for i in all_exercises:
        total = 0
        for item in exercises_data[i]:
            if item > 0.0:
                total += 1
        return_exercises[i] = {"mean": np.mean(exercises_data[i]), "median": np.median(exercises_data[i]),
                               "first": np.percentile(exercises_data[i], 25),
                               "last": np.percentile(exercises_data[i], 75), "passed": total}
    return return_exercises

def main():
    if check_param(sys.argv, 2):
        with open(sys.argv[1], 'r') as csvfile:
            reader = csv.reader(csvfile)
            data_csv = list(reader)
            if len(sys.argv) == 3:
                all_date, save_data = data_to_dict_per_id(data_csv)

                if sys.argv[2] == 'dates':
                    print(json.dumps(calc_date(all_date,save_data), ensure_ascii=False, indent=4))

                elif sys.argv[2] == 'deadlines':
                    print(json.dumps(calc_deadlines(all_date,save_data), ensure_ascii=False, indent=4))

                elif sys.argv[2] == 'exercises':
                    print(json.dumps(calc_exercises(save_data), ensure_ascii=False, indent=4))

                else:
                    print("Neplatny parametr")

if __name__ == "__main__":
    main()