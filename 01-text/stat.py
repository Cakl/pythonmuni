import sys
import re

parameter_ok = ok = 0
parameter_not_ok = 1
composers = []
dict_composers = {}
allow_parameters = ["century", "composer"]


def is_parameters_ok():
    if len(sys.argv) == 3 and sys.argv[2] in allow_parameters:
        return True
    else:
        return False


def is_composer():
    if sys.argv[2] == "composer":
        return True
    else:
        return False


def check_composer(file_name):
    runs_cnt = []
    for line in open(file_name, encoding="utf8"):
        match = re.match(r"^Composer:(.*)$", line)
        if match:
            #Delete brackets
            brackets_delete = re.sub(r'\([^)]*\)', '', match.group(1))
            for composer in brackets_delete.split(";"):
                composers.append(composer.strip())

    copy_composers = list(set(composers))

    for i in copy_composers:
        cnt = 0
        for j in composers:
            if i == j:
                cnt += 1

        runs_cnt.append(cnt)

    for cnt, f in enumerate(copy_composers):
            print("%s: %d" % (copy_composers[cnt], runs_cnt[cnt]))


def calc_from_year(date):
    if int(date[3]) >= 1 or int(date[2]) >= 1:
        return int(date[:2]) + 1
    else:
        return int(date[:2])


def check_century(file_name):
    centuries = []
    cent_cnt = []
    for line in open(file_name, encoding="utf8"):
        match = re.match(r"^Composition Year:(.*)$", line)
        if match:
            date = re.sub(r'[^\d]', ' ', match.group(1).strip()).strip()
            if len(date) == 4:
                centuries.append(calc_from_year(date))
            elif len(date) >= 10:
                centuries.append(calc_from_year(date[len(date)-4:]))
            elif len(date) == 2:
                centuries.append(int(date))

    copy_centuries = list(set(centuries))
    for i in copy_centuries:
        cnt = 0
        for j in centuries:
            if i == j:
                cnt += 1

        cent_cnt.append(cnt)

    for cnt, f in enumerate(copy_centuries):
            print("%dth century: %d" % (copy_centuries[cnt], cent_cnt[cnt]))


def main():
    if is_parameters_ok():
        if is_composer():
            check_composer(sys.argv[1])
            return ok
        else:
            check_century(sys.argv[1])
            return ok
    else:
        return parameter_not_ok


if __name__ == "__main__":
    main()
