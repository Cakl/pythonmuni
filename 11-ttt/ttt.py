from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import urlparse
from socketserver import ThreadingMixIn
import json
import sys


class ThreadingSimpleServer(ThreadingMixIn, HTTPServer):
    pass


def check_param(params, cnt_param):
    if len(params) == cnt_param + 1:
        return True
    else:
        return False


def check_path(path):
    # print(path)
    if path == '/status':
        return True
    elif path == '/start':
        return True
    elif path == '/play':
        return True

'''
all_game = {-1: None}
board = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
game_boards = {-1: board}
total_stat = {-1: '-'}
next = {-1: 1}
'''
all_game = {}
board = []
game_boards = {}
total_stat = {}
next = {}


def ret_game_id(id, s):
    """Respond to a GET request."""
    json_response = str(json.dumps({'id': id}, ensure_ascii=False, indent=4))
    json_response = bytes(json_response, 'utf-8')
    s.send_response(200)
    s.send_header("Content-type", "application/json")
    s.end_headers()
    s.wfile.write(json_response)


def ret_board_next(id, s):
    json_response = str(
        json.dumps({'board': game_boards[int(id)], 'next': next[int(id)]}, ensure_ascii=False, indent=1))
    json_response = bytes(json_response, 'utf-8')
    s.send_response(200)
    s.send_header("Content-type", "application/json")
    s.end_headers()
    s.wfile.write(json_response)


def ret_board_winner(id, s):
    json_response = str(json.dumps({'winner': total_stat[int(id)]}, ensure_ascii=False, indent=1))
    json_response = bytes(json_response, 'utf-8')
    s.send_response(200)
    s.send_header("Content-type", "application/json")
    s.end_headers()
    s.wfile.write(json_response)


def delete_game(id_game):
    del all_game[id_game]
    del game_boards[id_game]
    del total_stat[id_game]
    del next[id_game]


def CheckVictory(board, x, y):
    if board[0][y] == board[1][y] == board[2][y]:
        if board[0][y] == 0:
            return False, 0

        return True, board[0][y]

    if board[x][0] == board[x][1] == board[x][2]:
        if board[x][0] == 0:
            return False, 0
        return True, board[x][0]

    if x == y and board[0][0] == board[1][1] == board[2][2]:
        if board[0][0] == 0:
            return False, 0
        return True, board[0][0]

    if x + y == 2 and board[0][2] == board[1][1] == board[2][0]:
        return True, board[0][2]

    return False, 0


def check_finish(board, id):
    draw = 1
    for x in range(3):
        for y in range(3):
            if CheckVictory(board, x, y)[0]:
                total_stat[id] = CheckVictory(board, x, y)[1]
                return
    for x in range(3):
        for y in range(3):
            if board[x][y] == 0:
                draw = 0
                return
    if draw == 1:
        total_stat[id] = 0


def ret_bad_message(message, s):
    json_response = str(json.dumps({'status': 'bad', 'message': message}, ensure_ascii=False, indent=1))
    json_response = bytes(json_response, 'utf-8')
    s.send_response(400)
    s.send_header("Content-type", "application/json")
    s.end_headers()
    s.wfile.write(json_response)


def ret_bad_message_xy(message, s):
    json_response = str(json.dumps({'status': 'bad', 'message': message}, ensure_ascii=False, indent=1))
    json_response = bytes(json_response, 'utf-8')
    s.send_response(200)
    s.send_header("Content-type", "application/json")
    s.end_headers()
    s.wfile.write(json_response)


def ret_ok_message(s):
    json_response = str(json.dumps({'status': 'ok'}, ensure_ascii=False, indent=1))
    json_response = bytes(json_response, 'utf-8')
    s.send_response(200)
    s.send_header("Content-type", "application/json")
    s.end_headers()
    s.wfile.write(json_response)


def set_board(data_dict, s):
    if game_boards[int(data_dict['game'])][int(data_dict['x'])][int(data_dict['y'])] == 0:

        if int(data_dict['player']) == next[int(data_dict['game'])]:
            game_boards[int(data_dict['game'])][int(data_dict['x'])][int(data_dict['y'])] = int(data_dict['player'])

            check_finish(game_boards[int(data_dict['game'])], int(data_dict['game']))

            if int(data_dict['player']) == 1:
                next[int(data_dict['game'])] = 2
            else:
                next[int(data_dict['game'])] = 1

            status = 'ok'
            ret_ok_message(s)

        else:
            message = 'Bad player number'
            ret_bad_message_xy(message, s)

    else:
        message = 'Field is occupied'
        ret_bad_message_xy(message, s)


def ret_bad_game_char(s):
    message = 'Game id is int'
    ret_bad_message(message, s)


class Handler(BaseHTTPRequestHandler):
    def do_GET(s):
        global all_game
        global game_boards

        path = urlparse(s.path).path

        if len(all_game) > 0:
            last_id = list(all_game.keys())[-1]
        else:
            last_id = -1

        if check_path(path):
            game_id = None
            game_name = None

            if s.path == '/start':
                all_game[list(all_game.keys())[-1] + 1] = ''

                if last_id == 0:
                    delete_game(0)

                game_boards[last_id + 1] = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
                total_stat[last_id + 1] = '-'
                next[last_id + 1] = 1
                ret_game_id(last_id + 1, s)


            else:
                query = urlparse(s.path).query
                query_components = dict(qc.split("=") for qc in query.split("&"))


                if 'game' in query_components:

                    try:
                        int(query_components['game'])
                    except:
                        ret_bad_game_char(s)
                        return


                if len(query_components) == 1:
                    if "game" in query_components:
                        game_id = query_components['game']

                        if int(query_components['game']) in all_game:
                            if total_stat[int(game_id)] == '-':
                                ret_board_next(game_id, s)
                            else:
                                ret_board_winner(game_id, s)
                        else:
                            status = 'bad'
                            message = 'Game id is not exists!'
                            ret_bad_message(message, s)

                    elif "name" in query_components:
                        game_name = query_components['name']
                        all_game[last_id + 1] = game_name

                        if last_id == 0:
                            delete_game(0)

                        game_boards[last_id + 1] = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
                        total_stat[last_id + 1] = '-'
                        next[last_id + 1] = 1
                        ret_game_id(last_id + 1, s)
                elif len(query_components) == 4:
                    if 'game' in query_components:
                        if 'player' in query_components:
                            if 'x' in query_components:
                                if 'y' in query_components:

                                    try:
                                        int(query_components['x'])
                                        int(query_components['y'])
                                    except:
                                        message = 'X/Y must be int'
                                        ret_bad_message(message, s)
                                        return


                                    if int(query_components['game']) in all_game:
                                        if total_stat[int(query_components['game'])] == '-':
                                            if int(query_components['x']) > -1 and int(
                                                    query_components['x']) < 3 and int(
                                                    query_components['y']) > -1 and int(query_components['y']) < 3:
                                                if int(query_components['player']) > 0 and int(
                                                        query_components['player']) < 3:
                                                    status = 'ok'
                                                    set_board(query_components, s)
                                                else:
                                                    status = 'bad'
                                                    message = 'Player -> 1 or 2.'
                                            else:
                                                message = 'Parameter X or Y is not in range.'
                                                ret_bad_message_xy(message, s)
                                                return

                                        else:
                                            status = 'bad'
                                            message = 'Game was finished before..'
                                    else:
                                        status = 'bad'
                                        message = 'Game id is not exists!'
                                else:
                                    status = 'bad'
                                    message = 'Bad request parameter!'
                            else:
                                status = 'bad'
                                message = 'Bad request parameter!'
                        else:
                            status = 'bad'
                            message = 'Bad request parameter!'
                    else:
                        status = 'bad'
                        message = 'Bad request parameter!'
                    if status != 'ok':
                        ret_bad_message(message, s)


if __name__ == '__main__':

    if check_param(sys.argv, 1):
        httpd = ThreadingSimpleServer(('', int(sys.argv[1])), Handler)
        try:
            httpd.serve_forever()
        except KeyboardInterrupt:
            pass
        httpd.server_close()
