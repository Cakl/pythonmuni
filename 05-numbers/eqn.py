import sys
import re
import numpy as np
import collections

all_variables = []
dict_values = {}

def process_equ(data):
    results_int_only = []
    token_all = []
    all_detect_dim = []

    all_sort = []
    var_arr = []
    for item in data:
        final_var = []
        for var_i, variable in enumerate(item[1]):
            final_var.append(item[1][var_i])
        all_sort.append(final_var)


    for item in all_sort:
        for i in item:
            var_arr.append(i)

    variables = sorted(set(var_arr))

    for data_cnt,item in enumerate(data):
        final_number = []
        detect_dim = []
        id_all = []
        var = []
        dict_f = {}
        right_side = 0

        for var_i, i in enumerate(item[1]):
            if i != '/' and i != '_':
                if item[2][var_i] == '-':
                    detect_dim.append(-int(item[0][var_i]))
                else:
                    detect_dim.append(int(item[0][var_i]))
        all_detect_dim.append(detect_dim)

        for var_i, variable in enumerate(item[1]):
            if variable == '/':
                final_number.append(0)
                right_side = 1
            elif variable == '_' and right_side == 0:
                if item[2][var_i] == '+':
                    final_number.append(-int(item[0][var_i]))
                else:
                    final_number.append(int(item[0][var_i]))
            elif variable == '_' and right_side == 1:
                if item[2][var_i] == '+':
                    final_number.append(int(item[0][var_i]))
                else:
                    final_number.append(int(item[0][var_i]))
            else:
                if right_side == 0:
                    if item[2][var_i] == '-':
                        final_number.append(-int(item[0][var_i]))
                    else:
                        final_number.append(int(item[0][var_i]))
                else:
                    if item[2][var_i] == '-':
                        final_number.append(int(item[0][var_i]))
                    else:
                        final_number.append(-int(item[0][var_i]))

        for x in variables:
            ids = []
            if x not in var:
                var.append(x)
                for id_z,z in enumerate(all_sort[data_cnt]):
                    if x == z:
                        ids.append(id_z)
            id_all.append(ids)

        for i_it, it in enumerate(variables):
         dict_f[it] = id_all[i_it]

        od = collections.OrderedDict(sorted(dict_f.items()))

        for i in variables:
            int_res = 0
            for x in od[i]:
                int_res += final_number[x]

            results_int_only.append(int_res)
            token_all.append(i)

        results_int_only.append('#')
        token_all.append('#')

    only_coef = []
    only_coef_final = []
    only_const = []

    for cnt_item, item in enumerate(token_all):
        if item != '/' and item != '_' and item != '#':
            only_coef.append(results_int_only[cnt_item])

        elif item == '_':
            only_const.append(results_int_only[cnt_item])
        elif item == '#':
            only_coef_final.append(only_coef.copy())
            only_coef.clear()

    only_coef_mat = np.matrix(only_coef_final)

    for k in range(len(only_coef_final)):
        only_coef_final[k].append(only_const[k])
    ag_mat = np.matrix(only_coef_final)

    coef_rank = np.linalg.matrix_rank(only_coef_mat)
    var_rank = np.linalg.matrix_rank(ag_mat)

    o_var = []

    for item in variables:
        if item != '_' and item != '/':
            o_var.append(item)

    if coef_rank != var_rank:
        print("no solution")
    elif len(o_var) != coef_rank:
        print("solution space dimension: %d" % (len(o_var) - coef_rank))
    else:

        a = np.array(only_coef_mat)
        b = np.array(only_const)
        x = np.linalg.solve(a, b)
        str_fin = []
        for i_id, i in enumerate(o_var):
            str_fin.append(i + ' = ' + str(x[i_id]))

        fin = ''
        for i in str_fin:
            fin += i+', '
        print('solution: '+fin.rstrip(', '))


def parse_line(line):
    number_with_var_arr = []
    var_with_number_arr = []
    oper = []

    parse_equ = line.split()
    for cnt_i, i in enumerate(parse_equ):
        number_reg = re.match(r"^(\d*)$", i)
        var_reg = re.match(r"^([a-zA-Z])$", i)
        coef_reg = re.match(r"^(\d*)([a-zA-Z])$", i)

        if number_reg:
            if cnt_i == 0:
                oper.append('+')
            number_with_var_arr.append(number_reg.group(1))
            var_with_number_arr.append('_')
        elif var_reg:
            if cnt_i == 0:
                oper.append('+')
            number_with_var_arr.append(1)
            var_with_number_arr.append(var_reg.group(1))
            all_variables.append(var_reg.group(1))
        elif coef_reg:
            if cnt_i == 0:
                oper.append('+')
            number_with_var_arr.append(coef_reg.group(1))
            var_with_number_arr.append(coef_reg.group(2))
            all_variables.append(coef_reg.group(2))
        elif i == '+' or i == '-':
            oper.append(i)
        elif i == '=':
            number_with_var_arr.append('/')
            var_with_number_arr.append('/')
            oper.append('/')
            oper.append('+')

    for i in set(all_variables):
        if i not in var_with_number_arr:
            var_with_number_arr.append(i)
            number_with_var_arr.append(0)
            oper.append('+')
    return number_with_var_arr, var_with_number_arr, oper


def load(filename):
    datas = []
    for line in open(filename, encoding="utf8"):
        datas.append(parse_line(line))
    process_equ(datas)


def check_param(params, cnt_param):
    if len(params) == cnt_param + 1:
        return True
    else:
        return False


def main():
    if check_param(sys.argv, 1):
        load(sys.argv[1])


if __name__ == "__main__":
    main()