import http.server
import socketserver
import urllib.request
import json
import socket
import sys

URL = sys.argv[2]


def check_param(params, cnt_param):
    if len(params) == cnt_param + 1:
        return True
    else:
        return False


def check_port(port):
    if port < 9001:
        return False
    else:
        return True


def check_url():
    global URL
    if 'http://' not in URL:
        URL = 'http://' + URL


class Handler(http.server.SimpleHTTPRequestHandler):

    def do_GET(self):
        parse_jsn = None
        resp = {'code': ''}
        get_header = {}

        # for i in self.headers:
        #    if i != 'Host':
        #        get_header[i] = self.headers[i]
        check_url()
        request = urllib.request.Request(url=URL, data=None, method='GET', headers={})

        try:
            response = urllib.request.urlopen(request, timeout=1)
            code = response.getcode()
            r_header = {}

            for i in response.headers:
                r_header[i] = response.headers[i]

            resp['headers'] = r_header

            cont = str(response.read(), response.headers.get_content_charset(failobj="utf8"))

            try:
                parse_jsn = json.loads(cont)
            except:
                print('JSON error')

        except socket.timeout:
            code = 'timeout'

        resp['code'] = code

        if parse_jsn is not None:
            resp["json"] = parse_jsn
        else:
            resp["content"] = cont

        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.end_headers()
        self.wfile.write(bytes(json.dumps(resp, indent=4), "utf8"))

        return

    def do_POST(self):
        r_header = {}
        resp = {'code': ''}

        try:
            parse_jsn = json.loads(self.rfile.read(int(self.headers['Content-Length'])))
        except:
            resp['code'] = 'invalid json'

        try:
            inc_type = parse_jsn['type']
        except:
            inc_type = 'GET'

        try:
            inc_url = parse_jsn['url']
        except:
            inc_url=None

        try:
            inc_header = parse_jsn['headers']
        except:
            inc_header = {}
        try:
            inc_timeout = parse_jsn['timeout']
        except:
            inc_timeout = None
        try:
            inc_cont = parse_jsn['content']
        except:
            inc_cont = None

        try:
            inc_url = parse_jsn['url']
            if inc_type == 'POST':
                inc_cont = parse_jsn['content']
        except Exception:
            resp['code'] = 'invalid json'

        print(inc_header)
        if resp['code'] != 'invalid json':
            request = urllib.request.Request(inc_url, inc_type)

            try:
                response = urllib.request.urlopen(request, inc_timeout)
                for i in response.headers:
                    r_header[i] = response.headers[i]

                resp['headers'] = r_header

                cont = str(response.read(), response.headers.get_content_charset(failobj="utf8"))

                try:
                    resp['json'] = json.loads(cont.decode('utf-8'))
                except:
                    resp['content'] = cont.decode('utf-8')
            except:
                resp['code'] = 'timeout'

        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.end_headers()
        print(resp)
        self.wfile.write(bytes(json.dumps(resp, indent=4), "utf8"))

        return


def main():
    if check_param(sys.argv, 2) and check_port(int(sys.argv[1])):
        httpd = socketserver.TCPServer(('', int(sys.argv[1])), Handler)
        httpd.serve_forever()


if __name__ == "__main__":
    main()
