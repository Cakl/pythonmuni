import sys
import sqlite3
import json


def create_connection():
    try:
        conn = sqlite3.connect('scorelib.dat')
        return conn
    
    except:
        print("db_error")

    return None


def check_param(params, cnt_param):
    if len(params) == cnt_param + 1:
        return True
    else:
        return False


def find_composers(print_id):
    conn = create_connection()
    cur = conn.cursor()
    cur.execute("SELECT pri.id,per.name, per.born, per.died FROM print pri"
                " JOIN edition edi ON ( pri.edition = edi.id AND pri.id = ? )"
                " JOIN score sco ON ( sco.id = edi.score )"
                "JOIN score_author sca ON ( sca.score = sco.id  )"
                "JOIN person per ON (sca.composer = per.id )", (print_id,))
    rows = cur.fetchall()
    composers = []
    for composer in rows:
        composers.append({"Name": composer[1], "Born": composer[2], "Died": composer[3]})

    print(json.dumps(composers, indent=2, ensure_ascii=False))


def main():
    if check_param(sys.argv, 1):
        find_composers(sys.argv[1])


if __name__ == "__main__":
    main()