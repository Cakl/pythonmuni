import re

def extract_edition_name(data):
    for item in data:
        match = re.match(r"^Edition: (.*)$", item)
        if match:
            if match.group(1) == "":
                return None
            else:
                return match.group(1)


def extract_print_id(data):
    match = re.match(r"^Print Number: (.*)$", data)
    if match:
        return match.group(1)


def extract_partiture(data):
    for item in data:
        match = re.match(r"^Partiture: (.*)$", item)
        if match:
            if match.group(1).strip() == 'yes':
                return True
            else:
                return False


def extract_composition_name(data):
    for item in data:
        match = re.match(r"^Title: (.*)$", item)
        if match:
            if match.group(1) == "":
                return None
            else:
                return match.group(1)


def extract_composition_incipit(data):
    for item in data:
        match = re.match(r"^Incipit: (.*)$", item)
        if match:
            if match.group(1) == "":
                return None
            else:
                return match.group(1)


def extract_composition_key(data):
    for item in data:
        match = re.match(r"^Key: (.*)$", item)
        if match:
            if match.group(1) == "":
                return None
            else:
                return match.group(1)


def extract_composition_genre(data):
    for item in data:
        match = re.match(r"^Genre: (.*)$", item)
        if match:
            if match.group(1) == "":
                return None
            else:
                return match.group(1)

def extract_composition_year(data):
    for item in data:
        match = re.match(r"^Composition Year: (\d{4}.*)$", item)
        if match:
            if match.group(1) == "":
                return None

            else:
                return match.group(1)
        else:
            match2 = re.match(r"^Composition Year: (.*)$", item)
            if match2:
                return match2.group(1)

def extract_composition_voices(data):
    all_data = []
    for item in data:
        match = re.match(r"^Voice \d: (.*)$", item)
        if match:

            voice_match = re.match(r"^([A-Za-z0-9]*--[A-Za-z0-9]*),(.*)|(.*)$", match.group(1))
            if voice_match:
                if voice_match.group(1) is not None:
                    range = voice_match.group(1)
                else:
                    range = None

                if voice_match.group(2) is not None:
                    name = voice_match.group(2).lstrip()
                else:
                    name = None
                if voice_match.group(3) is not None:
                    name = voice_match.group(3).lstrip()
                    range = None
            else:
                range = None
                name = None
            all_data.append(Voice(name, range))
        else:
            match2 = re.match(r"^Voice \d:\n$", item)
            if match2:
                all_data.append(Voice(None, None))

    return all_data


def extract_composition_authors(data):
    all_data = []
    for item in data:
        match = re.match(r"^Composer: (.*)$", item)
        if match:
            composers = match.group(1).split(';')
            for composer in composers:
                brackets_delete = re.sub(r'(\([^)]*\))', '', composer)
                name = brackets_delete.lstrip().rstrip()
                match_born_die = re.findall(r"(\d{0,4})--(\d{0,4})", composer)
                match_born_die2 = re.findall(r"(\d{0,4})-(\d{0,4})", composer)

                if len(match_born_die) > 0:
                    for cnt_date,date in enumerate(match_born_die[0],1):
                        if cnt_date == 1:
                            if date != "":
                                born = date
                            else:
                                born = None
                        else:
                            if date != "":
                                died = date
                            else:
                                died = None
                elif len(match_born_die2) > 0:
                    for cnt_date,date in enumerate(match_born_die2[0],1):
                        if cnt_date == 1:
                            if date != "":
                                born = date+'-'
                            else:
                                born = None
                        else:
                            if date != "":
                                died = date+'-'
                            else:
                                died = None
                else:
                    born = None
                    died = None

                all_data.append(Person(name, born, died))
    return all_data


def extract_edition_author(data):
    all_data = []
    for item in data:
        match = re.match(r"^Editor: (.*)$", item)
        if match:
            if ',' in match.group(1):
                match_name = re.findall(r"([^\s]+\s*[^\s]+)", match.group(1))
            else:
                match_name = [match.group(1)]
            if match_name:
                for name in match_name:
                    all_data.append(Person(name))

    return all_data



class Voice:
    def __init__(self, name, range):
        self.name = name
        self.range = range

    def __repr__(self):
        return 'Class -> Voice:\n name: {}\n range: {}\n'.format(
            self.name, self.range
        )


class Person:
    def __init__(self, name='', born=None, died=None):
        self.name = name
        self.born = born
        self.died = died

    def __repr__(self):
        return 'Class -> Person:\n name: {}\n born: {}\n died: {}\n'.format(
            self.name, self.born, self.died
        )


class Composition:
    def __init__(self, data):
        self.name = extract_composition_name(data)
        self.incipit = extract_composition_incipit(data)
        self.key = extract_composition_key(data)
        self.genre = extract_composition_genre(data)
        self.year = extract_composition_year(data)
        self.voices = extract_composition_voices(data)
        self.authors = extract_composition_authors(data)

    def __repr__(self):
        return 'Class -> Composition:\n name: {}\n incipit: {}\n key: {}\n genre: {}\n year: {}\n voices: {}\n authors: {}'.format(
            self.name, self.incipit, self.key, self.genre, self.year, self.voices, self.authors
        )


class Edition:
    def __init__(self, data):
        self.name = extract_edition_name(data)
        self.composition = Composition(data)
        self.authors = extract_edition_author(data)

    def __repr__(self):
        return 'Class -> Edition:\n name: {}\n composition: {}\n authors: {}\n'.format(
            self.name, self.composition, self.authors
        )


class Print:
    def __init__(self, datas):
        self.print_id = extract_print_id(datas[0])
        self.partiture = extract_partiture(datas)
        self.edition = Edition(datas)



    def __repr__(self):
        return 'Class -> Print:\n print_id: {}\n partiture: {}\n edition: {}\n'.format(
            self.print_id, self.partiture, self.edition
        )

    def format(self):
        return_data = ""
        composition_author = ""
        editor_author = ""
        voices = ""
        for author in self.edition.composition.authors:
            if author.born is not None and author.died is not None:
                if "-" not in author.born and "-" not in author.died:
                    composition_author += author.name + ' ('+str(author.born)+'--'+str(author.died)+'); '
                else:
                    composition_author += author.name + ' (' + str(author.born.rstrip("-")) + '-' + str(author.died.rstrip("-")) + '); '
            elif author.born is None and author.died is None:
                composition_author += author.name + '; '
            elif author.born is not None and author.died is None:
                composition_author += author.name + ' ('+str(author.born)+'--''); '
            elif author.born is None and author.died is not None:
                composition_author += author.name + ' (''--'+str(author.died)+'); '

        for author in self.edition.authors:

            author_name = author.name.rstrip(",")
            editor_author += author_name + ', '

        for cnt_voice, voice in enumerate(self.edition.composition.voices,1):
            if voice.range is None:
                if voice.name is not None:
                    voices += "Voice {}: {}\n".format(cnt_voice, voice.name)
                else:
                    voices += "Voice {}:\n".format(cnt_voice)
            else:
                voices += "Voice {}: {}, {}\n".format(cnt_voice, voice.range, voice.name)


        return_data += "Print Number: {}\n" \
                       "Composer: {}\n" \
                       "Title: {}\n" \
                       "Genre: {}\n" \
                       "Key: {}\n" \
                       "Composition Year: {}\n" \
                       "Edition: {}\n" \
                       "Editor: {}\n" \
                       "{}\n" \
                       "Partiture: {}\n" \
                       "Incipit: {}\n".format(self.print_id, composition_author.rstrip("; "), self.edition.composition.name,
                                              "" if self.edition.composition.genre is None else self.edition.composition.genre, "" if self.edition.composition.key is None else self.edition.composition.key,
                                              "" if self.edition.composition.year is None else self.edition.composition.year, "" if self.edition.name is None else self.edition.name, editor_author.rstrip(", "),
                                              voices.rstrip("\n"), ("yes" if self.partiture else "no"), "" if self.edition.composition.incipit is None else self.edition.composition.incipit)

        return return_data



def load(filename):
    data_to_print = []
    data_saved = []
    for line in open(filename, encoding="utf8"):
        data_to_print.append(line)
        if line == '\n':
            if len(data_to_print) > 1:
                data_saved.append(Print(data_to_print))

            data_to_print.clear()


    data_saved.append(Print(data_to_print))

    return data_saved



