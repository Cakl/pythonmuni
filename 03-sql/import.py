import re
import sys
import sqlite3
import os


class Person:
    def __init__(self, name, born, died):
        self.name = name
        self.born = born
        self.died = died

def check_name(name):
    brackets_delete = re.sub(r'(\([^)]*\))', '', name)
    name_final = brackets_delete.lstrip().rstrip()
    return name_final

def person_editor_handler(person_name_reg, person_name_editor_reg):
    data_composers = []
    persona = []
    persona_check = []
    if person_name_editor_reg:
        brackets_delete = re.sub(r'\([^)]*\)', '', person_name_editor_reg.group(1))
        persona_edit = brackets_delete.split(',')
        for item in persona_edit:
            items_edit = clean(item)
            persona_check.append(items_edit)

        if len(persona_check) == 2:
            if len(persona_check[0].split(" ")) == 1:
                if len(persona_check[1].split(" ")) == 1:
                    persona.append(persona_check[0] + ' ' + persona_check[1])
                else:
                    persona.append(persona_check[0])
                    persona.append(persona_check[1])
            else:
                persona.append(persona_check[0])
                persona.append(persona_check[1])

        elif len(persona_edit) == 4:
            persona.append(persona_edit[0] + persona_edit[1])
            persona.append(persona_edit[2] + persona_edit[3])
        elif len(persona_edit) == 6:
            persona.append(persona_edit[0] + persona_edit[1])
            persona.append(persona_edit[2] + persona_edit[3])
            persona.append(persona_edit[4] + persona_edit[5])
        else:
            persona = persona_edit

        composers_editors = []
        for editor in persona:
            if editor:
                editor = editor.lstrip()
                editor = editor.rstrip()
                editor = 'E' + editor
                composers_editors.append(editor)
    else:
        composers_editors = []
        persona = person_name_reg.group(1).split(';')
        for composer in persona:
            if composer:
                composer = clean(composer)
                composer = 'C' + composer
                composers_editors.append(composer)

    for only_composer in composers_editors:
        name_born_die = re.match(r"^(.*,.*)\s\((.{4})-{1,2}(.{4})\)", only_composer)
        name_born_die_2 = re.match(r"^(.*,.*)\s\((.{4})-{1,2}(.{4}).*\)", only_composer)
        name_born_die_3 = re.match(r"^(.*,.*)\s\((.{4}).*-{1,2}(.{4}).*\)", only_composer)
        name_born_die_4 = re.match(r"^(.*)\s\((.{4}).*-{1,2}(.{4}).*\)", only_composer)
        name_born = re.match(r"^(.*,.*)\s\((.{4})--\)", only_composer)
        name_born_2 = re.match(r"^(.*)\s\((.{4})--\)", only_composer)
        name_died = re.match(r"^(.*, .*)\s\(\+(.{4})\)", only_composer)
        name_one = re.match(r"^(.*)", only_composer)

        if name_born_die:
            data_composers.append(
                Person(check_name(name_born_die.group(1)), name_born_die.group(2), name_born_die.group(3)))
        elif name_born_die_2:
            data_composers.append(
                Person(check_name(name_born_die_2.group(1)), name_born_die_2.group(2), name_born_die_2.group(3)))
        elif name_born_die_3:
            data_composers.append(
                Person(check_name(name_born_die_3.group(1)), name_born_die_3.group(2), name_born_die_3.group(3)))
        elif name_born_die_4:
            data_composers.append(
                Person(check_name(name_born_die_4.group(1)), name_born_die_4.group(2), name_born_die_4.group(3)))
        elif name_born:
            data_composers.append(Person(check_name(name_born.group(1)), name_born.group(2), None))
        elif name_died:
            data_composers.append(Person(check_name(name_died.group(1)), None, name_died.group(2)))
        elif name_born_2:
            data_composers.append(Person(check_name(name_born_2.group(1)), name_born_2.group(2), None))
        else:
            if name_one:
                data_composers.append(Person(check_name(name_one.group(1)), None, None))
            else:
                data_composers.append(Person(None, None, None))
    return data_composers


def create_connection(db_file):
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except:
        print("db_error")

    return None

def create_print(conn, print):
    sql = ''' INSERT INTO  print(id, partiture, edition) VALUES(?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql, print)
    return cur.lastrowid

def create_score(conn, score):
    sql = ''' INSERT INTO  score(name, genre, key, incipit, year) VALUES(?,?,?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql, score)
    return cur.lastrowid

def create_person(conn, person):
    sql = ''' INSERT INTO  person(born, died, name) VALUES(?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql, person)
    return cur.lastrowid

def create_edition(conn, edition):
    sql = ''' INSERT INTO  edition(score, name) VALUES(?,?) '''
    cur = conn.cursor()
    cur.execute(sql, edition)
    return cur.lastrowid

def create_score_author(conn, score_author):
    sql = ''' INSERT INTO  score_author(score, composer) VALUES(?,?) '''
    cur = conn.cursor()
    cur.execute(sql, score_author)
    return cur.lastrowid

def create_edition_author(conn, edition_author):
    sql = ''' INSERT INTO  edition_author(edition, editor) VALUES(?,?) '''
    cur = conn.cursor()
    cur.execute(sql, edition_author)
    return cur.lastrowid

def create_voice(conn, voice):
    sql = ''' INSERT INTO  voice(number, score, range, name) VALUES(?,?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql, voice)
    return cur.lastrowid

def find_person(conn, born, died, name):
    cur = conn.cursor()
    if born == None:
        born = ''
    if died == None:
        died = ''
    if name == None:
        name = ''
    person_id = None
    update = 0
    cur.execute(
        "SELECT id,born, died, name FROM person WHERE ifnull(name,'') =?",(name,))
    rows = cur.fetchall()


    if len(rows) > 0:
        person_id = rows[0][0]
        db_born = rows[0][1]
        db_died = rows[0][2]

        if db_born == None:
            if born != '':
                cur.execute("UPDATE person SET born=? WHERE id=?", (born, rows[0][0]))
                update = 1
        if db_died == None:
            if died != '':
                cur.execute("UPDATE person SET died=? WHERE id=?", (died, rows[0][0]))
                update = 1
    if update == 1:
        return (len(rows), rows[0][0])
    else:
        return (len(rows), person_id)


def find_score(conn, name, genre, key, incipit, year):
    cur = conn.cursor()
    if name == None:
        name = ''
    if genre == None:
        genre = ''
    if key == None:
        key = ''
    if incipit == None:
        incipit = ''
    if year == None:
        year = ''

    cur.execute("SELECT * FROM score WHERE ifnull(name,'') =? AND ifnull(genre,'') =? AND ifnull(key,'') =? AND ifnull(incipit,'')=? AND ifnull(year,'')=?", (name, genre, key, incipit, year,))
    rows = cur.fetchall()

    return rows


def find_edition(conn, score, edition_name):
    cur = conn.cursor()
    if score == None:
        score = ''

    if edition_name == None:
        edition_name = ''

    cur.execute("SELECT * FROM edition WHERE ifnull(score,'') =? AND ifnull(name,'') =?", (score, edition_name,))
    rows = cur.fetchall()

    return rows


def clean(data):
    update = data.lstrip()
    return update.rstrip()

def load(filename):
    conn = create_connection(sys.argv[2])
    print_ids = []
    persons = []
    voices_number = []
    voices_name = []
    voices_range = []
    score_name = None
    score_genre = None
    score_key = None
    score_incipit = None
    score_year = None

    num_lines = sum(1 for line in open(filename, encoding="utf8"))
    for xx, line in enumerate(open(filename, encoding="utf8")):

        if line == '\n' or xx + 1 == num_lines:
            if print_id in print_ids:
                continue

            score_data = (score_name,score_genre,score_key,score_incipit,score_year)
            if len(find_score(conn, score_name, score_genre, score_key, score_incipit, score_year)) == 0:
                score_id_row = create_score(conn, score_data)
                score_name = None
                score_genre = None
                score_key = None
                score_incipit = None
                score_year = None

            else:
                score_id_row = find_score(conn, score_name, score_genre, score_key, score_incipit, score_year)[0][0]

            edition_data = (score_id_row, editon_name)
            if len(find_edition(conn, score_id_row, editon_name)) == 0:
                edition_id_row = create_edition(conn, edition_data)

            else:
                edition_id_row = find_edition(conn, score_id_row, editon_name)[0][0]

            for person in persons:
                for person_item in person:
                    person_name = re.match(r"^(.)(.*)", person_item.name)

                    datas_person = find_person(conn, person_item.born, person_item.died, person_name.group(2))
                    if datas_person[0] == 0:
                        person_data = (person_item.born, person_item.died, person_name.group(2))
                        person_id_row = create_person(conn, person_data)

                        if (person_name.group(1) == 'C'):
                            if datas_person[1]:
                                score_author_data = (score_id_row, datas_person[1])
                                create_score_author(conn, score_author_data)
                            else:
                                score_author_data = (score_id_row, person_id_row)
                                create_score_author(conn, score_author_data)
                        else:
                            if datas_person[1]:
                                edition_author_data = (edition_id_row, datas_person[1])
                                create_edition_author(conn, edition_author_data)
                            else:
                                edition_author_data = (edition_id_row, person_id_row)
                                create_edition_author(conn, edition_author_data)
                    else:
                        if (person_name.group(1) == 'C'):
                            score_author_data = (score_id_row, datas_person[1])
                            create_score_author(conn, score_author_data)
                        else:
                            edition_author_data = (edition_id_row, datas_person[1])
                            create_edition_author(conn, edition_author_data)
            i = 0
            for number in voices_number:
                voice_data = (number, score_id_row, voices_range[i], voices_name[i])
                create_voice(conn, voice_data)
                i += 1

            persons.clear()
            voices_number.clear()
            voices_name.clear()
            voices_range.clear()

            print_data = (print_id, print_partiture, edition_id_row)
            print_id_row = create_print(conn, print_data)
            print_ids.append(print_id)

        conn.commit()

        print_id_reg = re.match(r"^Print Number: (.*)$", line)

        print_partiture_reg = re.match(r"^Partiture: (.*)$", line)

        edition_name_reg = re.match(r"^Edition: (.*)$", line)

        score_name_reg = re.match(r"^Title: (.*)$", line)
        score_genre_reg = re.match(r"^Genre: (.*)$", line)
        score_key_reg = re.match(r"^Key: (.*)$", line)
        score_incipit_reg = re.match(r"^Incipit: (.*)$", line)
        score_year_reg = re.match(r"^Composition Year: (.*)$", line)

        person_name_reg = re.match(r"^Composer: (.*)$", line)
        person_name_editor_reg = re.match(r"^Editor: (.*)$", line)

        voice_data_reg = re.match(r"^Voice (\d):\s{0,1}(.*)", line)

        if print_id_reg:
            print_id = clean(print_id_reg.group(1))


        if print_partiture_reg:
            print_partiture = clean(print_partiture_reg.group(1))
            print_partiture = re.sub(r'\([^)]*\)', '', print_partiture)
            if print_partiture == 'no':
                print_partiture = 'N'
            elif print_partiture == 'yes':
                print_partiture = 'Y'
            elif print_partiture == '':
                print_partiture = 'N'
            else:
                print_partiture = 'N'


        if edition_name_reg:
            if edition_name_reg.group(1) == '':
                editon_name = None
            else:
                editon_name = clean(edition_name_reg.group(1))

        if score_name_reg:
            if score_name_reg.group(1) == '':
                score_name = None
            else:
                score_name = clean(score_name_reg.group(1))

        if score_genre_reg:
            if score_genre_reg.group(1) == '':
                score_genre = None
            else:
                score_genre = clean(score_genre_reg.group(1))

        if score_key_reg:
            if score_key_reg.group(1) == '':
                score_key = None
            else:
                score_key = clean(score_key_reg.group(1))

        if score_incipit_reg:
            if score_incipit_reg.group(1) == '':
                score_incipit = None
            else:
                score_incipit = clean(score_incipit_reg.group(1))

        if score_year_reg:
            if score_year_reg.group(1) == '':
                score_year = None
            else:
                score_year = clean(score_year_reg.group(1))

        if person_name_reg or person_name_editor_reg:
            persons.append(person_editor_handler(person_name_reg, person_name_editor_reg))

        if voice_data_reg:
            voices_number.append(voice_data_reg.group(1))
            voices_range_reg = re.match(r"^(.*--[^,]*)", voice_data_reg.group(2))

            if voices_range_reg:
                voices_name_reg = re.match(r"%s,\s(.*)" % voices_range_reg.group(1), voice_data_reg.group(2))

                if voices_name_reg:
                    voices_name.append(voices_name_reg.group(1))
                else:
                    voices_name.append(None)

                voices_range.append(voices_range_reg.group(1))
            else:
                voices_name_reg = re.match(r"(.*)", voice_data_reg.group(2))

                if voices_name_reg:
                    voices_name.append(voices_name_reg.group(1))
                else:
                    voices_name_reg.append(None)
                voices_range.append(None)

            for item in voices_number:
                print(item)

    conn.close()

def main():
    os.system("sqlite3 {} < scorelib.sql".format(sys.argv[2]))
    load(sys.argv[1])


if __name__ == "__main__":
    main()
