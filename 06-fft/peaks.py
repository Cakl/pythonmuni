import sys
import wave
import struct
import numpy as np


def print_low_high(peak):
    print("low = %d, high = %d"% (min(peak), max(peak)))


def process_file(file):
    open_file = wave.open(file, 'r')
    cnt_channels = open_file.getnchannels()
    cnt_frames = open_file.getnframes()
    cnt_all_data = cnt_frames * cnt_channels

    fmt = ("%ih" % (cnt_all_data))

    all_channel = []
    int_datas = struct.unpack(fmt, open_file.readframes(cnt_frames))

    for cnt_item, item in enumerate(int_datas):
        j = cnt_item % cnt_channels
        all_channel.append([j,item])

    zero_chanels = []
    one_chanels = []


    for item in all_channel:
        if item[0] == 0:
            zero_chanels.append(item[1])
        else:
            one_chanels.append(item[1])


    sum = []
    for i in range(cnt_frames):
        if cnt_channels == 1:
            sum.append(zero_chanels[i])
        elif cnt_channels == 2:
            sum.append((zero_chanels[i] + one_chanels[i])/2)
        else:
            print("Unk_flag")

    wind = []
    cnt = 0
    peak = []
    frame_rate = open_file.getframerate()

    for item in sum:
        if cnt < frame_rate:
            wind.append(item)
            cnt+=1
        else:
            all_ampl = 0
            cnt = 0
            ampl_arr = []
            #amplitude of a complex number (abs)
            for val in np.fft.rfft(wind):
                ampl_arr.append(np.abs(val))
                all_ampl += np.abs(val)

            wind.clear()

            for cnt_datas, datas in enumerate(ampl_arr):
                summazition = 20 * (all_ampl/len(ampl_arr))
                if datas >= summazition:
                    peak.append(cnt_datas)
    return peak


def check_param(params, cnt_param):
    if len(params) == cnt_param + 1:
        return True
    else:
        return False


def main():
    if check_param(sys.argv, 1):
        data = process_file(sys.argv[1])
        
        if len(data) != 0:
            print_low_high(data)
        else:
            print("no peaks")


if __name__ == "__main__":
    main()